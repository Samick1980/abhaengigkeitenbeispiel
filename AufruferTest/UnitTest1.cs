﻿using AbhaengigkeitenBeispiel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace AufruferTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LeseDatenEin()
        {
            IAbhaengigkeit abhaengigkeit = Substitute.For<IAbhaengigkeit>();
            abhaengigkeit.Aktion().Returns("Das ist ein Test");
            abhaengigkeit.Berechne(1, 2).Returns(3);
            abhaengigkeit.Berechne(1, Arg.Any<int>()).Returns(-1);
            abhaengigkeit.Received().Aktion();
            abhaengigkeit.Aktion().Returns("Das", "ist", "ein", "Test");
            //Arrange
            Aufrufer aufrufer = new Aufrufer(abhaengigkeit);
            //Act
            aufrufer.RufeAuf();
            //Assert
            Assert.AreEqual(string.IsNullOrEmpty(aufrufer.Daten), false);
        }
    }

    class MockAbhaengigkeit : Abhaengigkeit
    {
        public override string Aktion()
        {
            return "Das ist ein Mock";
        }
    }

    class EigeneImplementierung : IAbhaengigkeit
    {
        public string Aktion()
        {
            return "Das ist ein Mock";
        }

        public int Berechne(int a, int b)
        {
            throw new System.NotImplementedException();
        }
    }
}
