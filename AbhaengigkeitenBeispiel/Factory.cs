﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbhaengigkeitenBeispiel
{
    class Factory
    {
        public IAbhaengigkeit createAbhaengigkeit()
        {
            return new Abhaengigkeit();
        }

        public IAufrufer createAufrufer()
        {
            return new Aufrufer(createAbhaengigkeit());
        }
    }
}
