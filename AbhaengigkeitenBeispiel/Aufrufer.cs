﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbhaengigkeitenBeispiel
{
    public class Aufrufer : IAufrufer
    {
        IAbhaengigkeit abhaengigkeit;

        // Hier werden Daten gespeichert
        public String Daten { get; set; }
        public Aufrufer(IAbhaengigkeit abh)
        {
            abhaengigkeit = abh;
        }
        public void RufeAuf()
        {
            // Werden Daten eingelesen
            Daten = abhaengigkeit.Aktion();
            // Daten werden ausgegeben
            Console.WriteLine(Daten);
        }
    }
}
