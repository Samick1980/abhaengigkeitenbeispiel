﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbhaengigkeitenBeispiel
{
    public interface IAufrufer
    {
        String Daten { get; set; }
        void RufeAuf();
    }
}
