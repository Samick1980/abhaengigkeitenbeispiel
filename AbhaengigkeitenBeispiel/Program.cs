﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace AbhaengigkeitenBeispiel
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType(typeof(IAbhaengigkeit), typeof(Abhaengigkeit));
            container.RegisterType(typeof(IAufrufer), typeof(Aufrufer));
            //var obj = container.Resolve(typeof(IAbhaengigkeit));
            //Factory factory = new Factory();
            //IAufrufer aufrufer = factory.createAufrufer();
            Aufrufer aufrufer = (Aufrufer)container.Resolve(typeof(IAufrufer));
            aufrufer.RufeAuf();
        }
    }
}
