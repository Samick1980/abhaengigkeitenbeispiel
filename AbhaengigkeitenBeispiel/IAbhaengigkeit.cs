﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbhaengigkeitenBeispiel
{
    public interface IAbhaengigkeit
    {
        string Aktion();
        int Berechne(int a, int b);
    }
}
